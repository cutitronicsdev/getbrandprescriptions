import logging
import json
import pymysql
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)

    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {
                           'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(400)
            else:

                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500



def fn_getPrescriptions(lv_CutitronicsBrandID):

    lv_list = []
    lv_stmt = "SELECT CutitronicsPresID, PrescriptionName, SkinType, CreationDate FROM BrandPrescriptions WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_stmt, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))

            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500



def lambda_handler(event, context):

    """
    getBrandPrescriptions
    """

    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_CutitronicsBrandID = None

    # Return block

    getBrandPrescriptions_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {"CutitronicsBrandID": None}}

    try:

        lv_Body = event.get('multiValueQueryStringParameters')

        logger.info(' ')
        logger.info("Payload body - '{body}'".format(body=lv_Body))
        logger.info(' ')

        lv_CutitronicsBrandID = lv_Body.get('CutitronicsBrandID')

        '''
        1. Does brand exist ?
        '''

        logger.info(' ')
        logger.info("Testing if Brand ID exists - '{lv_CutitronicsBrandID}'".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(' ')

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        if lv_BrandType == 400:  # Failure

            logger.error(' ')
            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))
            logger.error(' ')

            getBrandPrescriptions_out['Status'] = "Error"
            getBrandPrescriptions_out['ErrorCode'] = context.function_name + "_001"  # getAllTherapist_out_001
            getBrandPrescriptions_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"
            getBrandPrescriptions_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID

            # Lambda response

            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getBrandPrescriptions_out)
            }

        '''
        2. Return Brand Prescriptions
        '''

        lv_Prescriptions = fn_getPrescriptions(lv_CutitronicsBrandID)

        if lv_Prescriptions == 400:  # Failure

            logger.error(' ')
            logger.error("No Therapists found for Brand '{lv_CutitronicsBrandID}'".format(lv_CutitronicsBrandID=lv_CutitronicsBrandID))
            logger.error(' ')

            getBrandPrescriptions_out['Status'] = "Success"
            getBrandPrescriptions_out['ErrorCode'] = context.function_name + "_002"  # getAllTherapist_out_001
            getBrandPrescriptions_out['ErrorDesc'] = "No Prescriptions found"
            getBrandPrescriptions_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID

            # Lambda response

            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getBrandPrescriptions_out)
            }

        # Create output

        getBrandPrescriptions_out['Status'] = "Success"
        getBrandPrescriptions_out['ErrorCode'] = context.function_name + "_000"
        getBrandPrescriptions_out['ErrorDesc'] = ""
        getBrandPrescriptions_out['ServiceOutput']['CutitronicsBrandID'] = lv_CutitronicsBrandID

        PrescriptionList = []
        PresDict = {}

        for lv_Prescription in lv_Prescriptions:

            PresDict = {
                "CutitronicsPresID": lv_Prescription.get('CutitronicsPresID'),
                "PrescriptionName": lv_Prescription.get('PrescriptionName'),
                "SkinType": lv_Prescription.get('SkinType'),
                "CreationDate": lv_Prescription.get('SkinType')
            }

            PrescriptionList.append(PresDict)

        getBrandPrescriptions_out['ServiceOutput']['Prescriptions'] = PrescriptionList

        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getBrandPrescriptions_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        # Populate Cutitronics reply block

        getBrandPrescriptions_out['Status'] = "Error"
        getBrandPrescriptions_out['ErrorCode'] = context.function_name + "_000"  # regUser_000
        getBrandPrescriptions_out['ErrorDesc'] = "CatchALL"
        getBrandPrescriptions_out['ServiceOutput']['brandID'] = lv_CutitronicsBrandID

        # Lambda response

        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getBrandPrescriptions_out)
        }
